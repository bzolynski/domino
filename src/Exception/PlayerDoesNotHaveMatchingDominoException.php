<?php

declare(strict_types=1);

namespace CODEfactors\Domino\Exception;

use Exception;

class PlayerDoesNotHaveMatchingDominoException extends Exception
{

}
