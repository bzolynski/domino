<?php

declare(strict_types=1);

namespace CODEfactors\Domino\Policy;

use CODEfactors\Domino\Board;
use CODEfactors\Domino\Dominoes;
use CODEfactors\Domino\TileOwner\PlayerOwner;

class NoWinnersPolicy
{
    public static function isSatisfiedBy(Dominoes $dominoes, Board $board, PlayerOwner ...$playerOwners): bool
    {
        foreach ($playerOwners as $playerOwner) {
            foreach ($dominoes->getForPlayer($playerOwner) as $dominoTile) {
                if ($board->matches($dominoTile)) {
                    return false;
                }
            }
        }

        return !$dominoes->anyTilesOnStackLeft();
    }
}
