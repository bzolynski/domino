<?php

declare(strict_types=1);

namespace CODEfactors\Domino\Policy;

use CODEfactors\Domino\DominoTile;

class PlayerStartingDominoTilesPolicy
{
    private $startingDominoTiles;

    public function __construct(int $numberOfPlayers)
    {
        $this->startingDominoTiles = 5;
        if ($numberOfPlayers === 2) {
            $this->startingDominoTiles = 7;
        }
    }

    public function isSatisfiedBy(DominoTile ...$dominoTiles): bool
    {
        return count($dominoTiles) === $this->startingDominoTiles;
    }
}
