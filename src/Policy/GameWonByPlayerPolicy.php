<?php

declare(strict_types=1);

namespace CODEfactors\Domino\Policy;

use CODEfactors\Domino\Dominoes;
use CODEfactors\Domino\TileOwner\PlayerOwner;

class GameWonByPlayerPolicy
{
    public static function isSatisfiedBy(Dominoes $dominoes, PlayerOwner $playerOwner): bool
    {
        return !$dominoes->anyTilesOwnedByPlayer($playerOwner);
    }
}
