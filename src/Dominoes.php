<?php

declare(strict_types=1);

namespace CODEfactors\Domino;

use CODEfactors\Domino\Exception\InvalidFieldValueException;
use CODEfactors\Domino\Exception\NoMoreTilesOnStackException;
use CODEfactors\Domino\TileOwner\PlayerOwner;
use CODEfactors\Domino\TileOwner\StackTileOwner;

class Dominoes
{
    private const FIELD_TYPES = [null, 1, 2, 3, 4, 5, 6];

    /**
     * @var DominoTile[]
     */
    private $tiles = [];

    /**
     * @throws InvalidFieldValueException
     */
    public function __construct()
    {
        $this->createAllCombinations();
    }

    /**
     * @throws NoMoreTilesOnStackException
     */
    public function drawTileFromStack(): DominoTile
    {
        $availableTiles = $this->getAllTilesAvailableOnStack();
        if (count($availableTiles) === 0) {
            throw new NoMoreTilesOnStackException();
        }
        $tileToPickIndex = rand(0, count($availableTiles) - 1);

        return $availableTiles[$tileToPickIndex];
    }

    /**
     * @return DominoTile[]
     */
    public function getForPlayer(PlayerOwner $playerOwner): array
    {
        $availableTiles = [];
        foreach ($this->tiles as $tile) {
            if ($tile->isOwnedByPlayer($playerOwner)) {
                $availableTiles[] = $tile;
            }
        }

        return $availableTiles;
    }

    public function anyTilesOnStackLeft(): bool
    {
        foreach ($this->tiles as $tile) {
            if ($tile->isOnStack()) {
                return true;
            }
        }

        return false;
    }

    public function anyTilesOwnedByPlayer(PlayerOwner $playerOwner): bool
    {
        foreach ($this->tiles as $tile) {
            if ($tile->isOwnedByPlayer($playerOwner)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @throws InvalidFieldValueException
     */
    private function createAllCombinations(): void
    {
        for ($i = 0; $i < count(self::FIELD_TYPES); $i++) {
            for ($j = 0; $j <= $i; $j++) {
                $this->tiles[] = new DominoTile(self::FIELD_TYPES[$i], self::FIELD_TYPES[$j], new StackTileOwner());
            }
        }
    }

    /**
     * @return DominoTile[]
     */
    private function getAllTilesAvailableOnStack(): array
    {
        $availableTiles = [];
        foreach ($this->tiles as $tile) {
            if ($tile->isOnStack()) {
                $availableTiles[] = $tile;
            }
        }

        return $availableTiles;
    }
}
