<?php

declare(strict_types=1);

namespace CODEfactors\Domino;

use CODEfactors\Domino\TileOwner\PlayerOwner;

class MessageLog implements MessageLogInterface
{
    private $messages = [];

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    public function logInitialBoardStatusMessage(Board $board): void
    {
        $tiles = $board->getTiles();

        $this->addMessage(
            sprintf('Game starts with first tile: %s', $tiles[0]->toString())
        );
    }

    public function logBoardStatusMessage(Board $board): void
    {
        $this->addMessage(
            'Board is now: ' . join(' ', array_map(
                function(DominoTile $dominoTile): string {
                    return $dominoTile->toString();
                }, $board->getTiles()
            ))
        );
    }

    public function logDominoTileConnectedMessage(
        PlayerOwner $playerOwner,
        DominoTile $dominoTile,
        DominoTile $dominoTileToJoinWith
    ): void {
        $this->addMessage(
            sprintf(
                '%s plays %s to connect to tile %s on the board',
                $playerOwner->getName(),
                $dominoTile->toString(),
                $dominoTileToJoinWith->toString()
            )
        );
    }

    public function logPlayerDrawsTileFromStack(PlayerOwner $playerOwner, DominoTile $dominoTile): void
    {
        $this->addMessage(
            sprintf('%s can\'t play, draws tile %s', $playerOwner->getName(), $dominoTile->toString())
        );
    }

    public function logGameWonByPlayer(PlayerOwner $playerOwner): void
    {
        $this->addMessage(
            sprintf('Player %s has won!', $playerOwner->getName())
        );
    }

    public function logNoWinner(Dominoes $dominoes, PlayerOwner ...$playerOwners): void
    {
        foreach ($playerOwners as $playerOwner) {
            $dominoTiles = array_map(function(DominoTile $dominoTile): string {
                return $dominoTile->toString();
            }, $dominoes->getForPlayer($playerOwner));
            $this->addMessage(
                sprintf('Player %s has %s', $playerOwner->getName(), join(', ', $dominoTiles))
            );
        }
        $this->addMessage('No winner');
    }

    private function addMessage(string $message): void
    {
        $this->messages[] = $message;
    }
}
