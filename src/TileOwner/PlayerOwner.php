<?php

declare(strict_types=1);

namespace CODEfactors\Domino\TileOwner;

class PlayerOwner implements TileOwnerInterface
{
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isEqualTo(PlayerOwner $playerOwner): bool
    {
        return $this->name === $playerOwner->getName();
    }
}
