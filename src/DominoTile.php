<?php

declare(strict_types=1);

namespace CODEfactors\Domino;

use CODEfactors\Domino\Exception\InvalidFieldValueException;
use CODEfactors\Domino\TileOwner\BoardTileOwner;
use CODEfactors\Domino\TileOwner\PlayerOwner;
use CODEfactors\Domino\TileOwner\StackTileOwner;
use CODEfactors\Domino\TileOwner\TileOwnerInterface;

class DominoTile
{
    private $firstField;

    private $secondField;

    private $owner;

    /**
     * @throws InvalidFieldValueException
     */
    public function __construct(?int $firstField, ?int $secondField, TileOwnerInterface $tileOwner)
    {
        if (!$this->isFieldValid($firstField) || !$this->isFieldValid($secondField)) {
            throw new InvalidFieldValueException();
        }

        $this->firstField = $firstField;
        $this->secondField = $secondField;
        $this->owner = $tileOwner;
    }

    public function getFirstField(): ?int
    {
        return $this->firstField;
    }

    public function getSecondField(): ?int
    {
        return $this->secondField;
    }

    public function swap(): void
    {
        $swappedField = $this->firstField;
        $this->firstField = $this->secondField;
        $this->secondField = $swappedField;
    }

    public function matchesWith(?int $field): bool
    {
        return $this->isFirstFieldEqualTo($field) || $this->isSecondFieldEqualTo($field);
    }

    public function toString(): string
    {
        return sprintf('<%d:%d>', $this->firstField, $this->secondField);
    }

    public function setOnBoard(): void
    {
        $this->owner = new BoardTileOwner();
    }

    public function setOnPlayerPile(PlayerOwner $playerOwner): void
    {
        $this->owner = $playerOwner;
    }

    public function isOnStack(): bool
    {
        return $this->owner instanceof StackTileOwner;
    }

    public function isOwnedByPlayer(PlayerOwner $playerOwner): bool
    {
        return $this->owner instanceof PlayerOwner && $this->owner->isEqualTo($playerOwner);
    }

    private function isFieldValid(?int $field): bool
    {
        return is_null($field) || ($field >= 1 && $field <= 6);
    }

    private function isFirstFieldEqualTo(?int $field): bool
    {
        return $this->firstField === $field;
    }

    private function isSecondFieldEqualTo(?int $field): bool
    {
        return $this->secondField === $field;
    }
}
