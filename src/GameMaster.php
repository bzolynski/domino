<?php

declare(strict_types=1);

namespace CODEfactors\Domino;

use CODEfactors\Domino\Exception\InvalidNumberOfPlayersException;
use CODEfactors\Domino\Exception\NoMoreTilesOnStackException;
use CODEfactors\Domino\Exception\NonUniquePlayerNameException;
use CODEfactors\Domino\Exception\PlayerDoesNotHaveMatchingDominoException;
use CODEfactors\Domino\Policy\GameWonByPlayerPolicy;
use CODEfactors\Domino\Policy\NoWinnersPolicy;
use CODEfactors\Domino\Policy\PlayerStartingDominoTilesPolicy;
use CODEfactors\Domino\TileOwner\PlayerOwner;

class GameMaster
{
    private $dominoes;

    /**
     * @var PlayerOwner[]
     */
    private $players = [];

    /**
     * @var PlayerStartingDominoTilesPolicy
     */
    private $startingTilesPolicy;

    /**
     * @var Board
     */
    private $board;

    private $playerTurn = 0;

    private $messageLog;

    private $winner = null;

    /**
     * @throws InvalidNumberOfPlayersException
     * @throws NoMoreTilesOnStackException
     * @throws NonUniquePlayerNameException
     */
    public function __construct(Dominoes $dominoes, MessageLogInterface $messageLog, string ...$playerNames)
    {
        if (count($playerNames) < 2) {
            throw new InvalidNumberOfPlayersException();
        }
        $this->validatePlayers(...$playerNames);
        $this->dominoes = $dominoes;
        $this->messageLog = $messageLog;
        $this->determineNumberOfStartingDominoTiles(count($playerNames));
        $this->drawStartingDominoesForAllPlayers(...$playerNames);
        $this->putFirstDominoOnTheBoard();
        $this->messageLog->logInitialBoardStatusMessage($this->board);
    }

    public function continue(): void
    {
        $playerOwner = $this->players[$this->playerTurn];
        do {
            try {
                $dominoTileToJoinWith = $this->putMatchingDominoForPlayerOnTheBoard($playerOwner);
                if (GameWonByPlayerPolicy::isSatisfiedBy($this->dominoes, $playerOwner)) {
                    $this->messageLog->logBoardStatusMessage($this->board);
                    $this->gameWonByPlayer($playerOwner);

                    return;
                }
            } catch (PlayerDoesNotHaveMatchingDominoException $exception) {
                try {
                    $dominoTile = $this->dominoes->drawTileFromStack();
                    $dominoTile->setOnPlayerPile($playerOwner);
                    $this->messageLog->logPlayerDrawsTileFromStack($playerOwner, $dominoTile);
                    $dominoTileToJoinWith = null;
                } catch (NoMoreTilesOnStackException $exception) {
                    $this->nextTurn();

                    return;
                }
            }

        } while ($dominoTileToJoinWith === null);

        $this->messageLog->logBoardStatusMessage($this->board);
        $this->nextTurn();
    }

    public function isGameFinished(): bool
    {
        return NoWinnersPolicy::isSatisfiedBy($this->dominoes, $this->board, ...$this->players) || $this->getWinner() !== null;
    }

    public function getWinner(): ?PlayerOwner
    {
        return $this->winner;
    }

    /**
     * @throws NonUniquePlayerNameException
     */
    private function validatePlayers(string ...$playerNames): void
    {
        foreach ($playerNames as $playerName) {
            if (count(array_keys($playerNames, $playerName)) > 1) {
                throw new NonUniquePlayerNameException();
            }
        }
    }

    private function determineNumberOfStartingDominoTiles(int $numberOfPlayers): void
    {
        $this->startingTilesPolicy = new PlayerStartingDominoTilesPolicy($numberOfPlayers);
    }

    /**
     * @throws NoMoreTilesOnStackException
     */
    private function putFirstDominoOnTheBoard(): void
    {
        $tile = $this->dominoes->drawTileFromStack();
        $tile->setOnBoard();
        $this->board = new Board($tile);
    }

    /**
     * @throws NoMoreTilesOnStackException
     */
    private function drawStartingDominoesForAllPlayers(string ...$playerNames): void
    {
        foreach ($playerNames as $playerName) {
            $playerOwner = new PlayerOwner($playerName);
            $this->players[] = $playerOwner;
            $this->drawStartingTilesForPlayer($playerOwner);
        }
    }

    /**
     * @throws NoMoreTilesOnStackException
     */
    private function drawStartingTilesForPlayer(PlayerOwner $playerOwner): void
    {
        $tiles = [];
        do {
            $tile = $this->dominoes->drawTileFromStack();
            $tile->setOnPlayerPile($playerOwner);
            $tiles[] = $this->dominoes->drawTileFromStack();
        } while (!$this->startingTilesPolicy->isSatisfiedBy(...$tiles));
    }

    /**
     * @throws PlayerDoesNotHaveMatchingDominoException
     */
    private function putMatchingDominoForPlayerOnTheBoard(PlayerOwner $playerOwner): DominoTile
    {
        $playerDominoes = $this->dominoes->getForPlayer($playerOwner);
        foreach ($playerDominoes as $dominoTile) {
            if ($this->board->matches($dominoTile)) {
                $dominoTile->setOnBoard();
                $dominoTileToJoinWith = $this->board->addDominoTile($dominoTile);

                $this->messageLog->logDominoTileConnectedMessage($playerOwner, $dominoTile, $dominoTileToJoinWith);

                return $dominoTileToJoinWith;
            }
        }

        throw new PlayerDoesNotHaveMatchingDominoException();
    }

    private function nextTurn(): void
    {
        if (NoWinnersPolicy::isSatisfiedBy($this->dominoes, $this->board, ...$this->players)) {
            $this->messageLog->logNoWinner($this->dominoes, ...$this->players);

            return;
        }

        $this->playerTurn += 1;
        if ($this->playerTurn > count($this->players) - 1) {
            $this->playerTurn = 0;
        }
    }

    private function gameWonByPlayer(PlayerOwner $playerOwner): void {
        $this->winner = $playerOwner;
        $this->messageLog->logGameWonByPlayer($playerOwner);
    }
}
