<?php

declare(strict_types=1);

namespace CODEfactors\Domino;

use CODEfactors\Domino\Exception\PlayerDoesNotHaveMatchingDominoException;

class Board
{
    private $tiles = [];

    private $firstTileEnd;

    private $secondTileEnd;

    public function __construct(DominoTile $dominoTile)
    {
        $this->tiles[] = $dominoTile;
        $this->firstTileEnd = $dominoTile->getFirstField();
        $this->secondTileEnd = $dominoTile->getSecondField();
    }

    /**
     * @return DominoTile[]
     */
    public function getTiles(): array
    {
        return $this->tiles;
    }

    public function getFirstTileEnd(): ?int
    {
        return $this->firstTileEnd;
    }

    public function getSecondTileEnd(): ?int
    {
        return $this->secondTileEnd;
    }

    public function matches(DominoTile $dominoTile): bool
    {
        return $dominoTile->matchesWith($this->firstTileEnd) || $dominoTile->matchesWith($this->secondTileEnd);
    }

    public function addDominoTile(DominoTile $dominoTile): DominoTile
    {
        if ($dominoTile->matchesWith($this->firstTileEnd)) {
            return $this->setTileOnTheBeginning($dominoTile);
        }

        return $this->setTileOnTheEnd($dominoTile);
    }

    private function setTileOnTheBeginning(DominoTile $dominoTile): DominoTile
    {
        if ($this->firstTileEnd === $dominoTile->getFirstField()) {
            $dominoTile->swap();
        }

        $this->firstTileEnd = $dominoTile->getFirstField();

        $dominoTileToJoinWith = reset($this->tiles);
        array_unshift($this->tiles, $dominoTile);

        return $dominoTileToJoinWith;
    }

    private function setTileOnTheEnd(DominoTile $dominoTile): DominoTile
    {
        if ($this->secondTileEnd === $dominoTile->getSecondField()) {
            $dominoTile->swap();
        }

        $this->secondTileEnd = $dominoTile->getSecondField();

        $dominoTileToJoinWith = end($this->tiles);
        $this->tiles[] = $dominoTile;

        return $dominoTileToJoinWith;
    }
}
