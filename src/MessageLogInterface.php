<?php

declare(strict_types=1);

namespace CODEfactors\Domino;

use CODEfactors\Domino\TileOwner\PlayerOwner;

interface MessageLogInterface
{
    public function getMessages(): array;

    public function logInitialBoardStatusMessage(Board $board): void;

    public function logBoardStatusMessage(Board $board): void;

    public function logDominoTileConnectedMessage(
        PlayerOwner $playerOwner,
        DominoTile $dominoTile,
        DominoTile $dominoTileToJoinWith
    ): void;

    public function logPlayerDrawsTileFromStack(PlayerOwner $playerOwner, DominoTile $dominoTile): void;

    public function logGameWonByPlayer(PlayerOwner $playerOwner): void;

    public function logNoWinner(Dominoes $dominoes, PlayerOwner ...$playerOwners): void;
}
