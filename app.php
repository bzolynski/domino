<?php

declare(strict_types=1);

namespace CODEfactors\Domino;

require_once('vendor/autoload.php');

$messageLog = new MessageLog();

$gameMaster = new GameMaster(new Dominoes(), $messageLog, ...['Alice', 'Bob']);

while (!$gameMaster->isGameFinished()) {
    $gameMaster->continue();
}

foreach ($messageLog->getMessages() as $message) {
    echo $message . PHP_EOL;
}
