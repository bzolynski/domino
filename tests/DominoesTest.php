<?php

declare(strict_types=1);

namespace CODEfactors\Tests\Domino;

use CODEfactors\Domino\Dominoes;
use CODEfactors\Domino\DominoTile;
use CODEfactors\Domino\Exception\NoMoreTilesOnStackException;
use PHPUnit\Framework\TestCase;

final class DominoesTest extends TestCase
{
    private const ALL_TILE_COMBINATIONS =
        [
            [null, null],
            [null, 1], [1, 1],
            [null, 2], [1, 2], [2, 2],
            [null, 3], [1, 3], [2, 3], [3, 3],
            [null, 4], [1, 4], [2, 4], [3, 4], [4, 4],
            [null, 5], [1, 5], [2, 5], [3, 5], [4, 5], [5, 5],
            [null, 6], [1, 6], [2, 6], [3, 6], [4, 6], [5, 6], [6, 6],
        ];

    /**
     * @test
     */
    public function allCombinationsOfTilesHaveBeenCorrectlyCreated(): void
    {
        $dominoes = new Dominoes();
        $actualTiles = $this->retrieveAllTilesFromStack($dominoes);
        foreach (self::ALL_TILE_COMBINATIONS as $expectedTileFields) {
            $doesTileExist = false;
            foreach ($actualTiles as $actualTile) {
                if ($this->firstAndSecondFieldEquals($actualTile, $expectedTileFields) ||
                    ($this->secondAndFirstFieldEquals($actualTile, $expectedTileFields))) {

                    $doesTileExist = true;
                    continue;
                }
            }
            $this->assertTrue(
                $doesTileExist,
                sprintf('Expected tile <%d:%d> not found on stack', $expectedTileFields[0], $expectedTileFields[1])
            );
        }
    }

    /**
     * @test
     */
    public function noMoreTilesOnStack(): void
    {
        $this->expectException(NoMoreTilesOnStackException::class);
        $dominoes = new Dominoes();
        for ($i = 1; $i <= count(self::ALL_TILE_COMBINATIONS) + 1; $i++) {
            $dominoTile = $dominoes->drawTileFromStack();
            $dominoTile->setOnBoard();
            if ($i === count(self::ALL_TILE_COMBINATIONS)) {
                $this->assertFalse($dominoes->anyTilesOnStackLeft());
            } else {
                $this->assertTrue($dominoes->anyTilesOnStackLeft());
            }
        }
    }

    /**
     * @throws NoMoreTilesOnStackException
     * @return DominoTile[]
     */
    private function retrieveAllTilesFromStack(Dominoes $dominoes): array
    {
        $actualDominoTiles = [];
        for ($i = 1; $i <= count(self::ALL_TILE_COMBINATIONS); $i++) {
            $dominoTile = $dominoes->drawTileFromStack();
            $dominoTile->setOnBoard();
            $actualDominoTiles[] = $dominoTile;
        }

        return $actualDominoTiles;
    }

    private function firstAndSecondFieldEquals(DominoTile $actualTile, array $expectedTileFields): bool
    {
        return $actualTile->getFirstField() === $expectedTileFields[0] &&
            $actualTile->getSecondField() === $expectedTileFields[1];
    }

    private function secondAndFirstFieldEquals(DominoTile $actualTile, array $expectedTileFields): bool
    {
        return $actualTile->getSecondField() === $expectedTileFields[0] &&
            $actualTile->getFirstField() === $expectedTileFields[1];
    }
}
