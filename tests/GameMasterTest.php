<?php

declare(strict_types=1);

namespace CODEfactors\Tests\Domino;

use CODEfactors\Domino\Board;
use CODEfactors\Domino\Dominoes;
use CODEfactors\Domino\DominoTile;
use CODEfactors\Domino\Exception\InvalidNumberOfPlayersException;
use CODEfactors\Domino\Exception\NonUniquePlayerNameException;
use CODEfactors\Domino\GameMaster;
use CODEfactors\Domino\MessageLogInterface;
use CODEfactors\Domino\TileOwner\PlayerOwner;
use CODEfactors\Domino\TileOwner\StackTileOwner;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionProperty;

final class GameMasterTest extends TestCase
{
    /**
     * @var ReflectionProperty
     */
    private $dominoTilesProperty;

    /**
     * @var ReflectionProperty
     */
    private $dominoTileOwnerProperty;

    /**
     * @var ReflectionProperty
     */
    private $boardProperty;

    /**
     * @var MessageLogInterface|MockObject
     */
    private $messageLogMock;

    /**
     * @var Dominoes
     */
    private $dominoes;

    /**
     * @var GameMaster
     */
    private $gameMaster;

    private const PLAYER_1 = 'Alice';

    private const PLAYER_2 = 'Bob';

    private const PLAYER_TILES = [
        self::PLAYER_1 => [
            [5, null],
            [null, 6],
        ],
        self::PLAYER_2 => [
            [5, 5],
            [3, 5],
        ],
    ];

    private const STACK_TILES = [
        [4, 1]
    ];

    public function setUp(): void
    {
        $this->messageLogMock = $this->createMock(MessageLogInterface::class);
        $this->setDominoTilesReflectionProperty();
        $this->setDominoTileOwnerReflectionProperty();
        $this->setBoardReflectionProperty();
        $this->dominoes = new Dominoes();
        $this->gameMaster = new GameMaster($this->dominoes, $this->messageLogMock, ...[self::PLAYER_1, self::PLAYER_2]);
    }

    /**
     * @test
     */
    public function throwsAnExceptionWhenNumberOfPlayersIsLessThanTwo(): void
    {
        $this->expectException(InvalidNumberOfPlayersException::class);
        new GameMaster($this->dominoes, $this->messageLogMock, ...[self::PLAYER_1]);
    }

    /**
     * @test
     */
    public function gameShouldStartWithProperAmountOfDominoTilesForTwoPlayers(): void
    {
        $dominoTiles = $this->dominoes->getForPlayer(new PlayerOwner(self::PLAYER_1));
        $this->assertCount(7, $dominoTiles);
    }

    /**
     * @test
     */
    public function gameShouldStartWithProperAmountOfDominoTilesForMoreThanTwoPlayers(): void
    {
        $dominoes = new Dominoes();
        new GameMaster(
            $dominoes,
            $this->messageLogMock,
            ...[self::PLAYER_1, self::PLAYER_2, 'EXTRA_PLAYER']
        );
        $dominoTiles = $dominoes->getForPlayer(new PlayerOwner(self::PLAYER_1));
        $this->assertCount(5, $dominoTiles);
    }

    /**
     * @test
     */
    public function tileEndsOnTheBoardShouldBeSetProperly(): void
    {
        $firstPlayer = new PlayerOwner(self::PLAYER_1);
        $secondPlayer = new PlayerOwner(self::PLAYER_2);

        // Board is now: <1:6>
        $board = new Board(new DominoTile(1, 6, new StackTileOwner()));
        /** @var DominoTile[] $dominoTiles */
        $dominoTiles = $this->dominoTilesProperty->getValue($this->dominoes);
        foreach ($dominoTiles as $dominoTile) {
            if ($this->assignTileToPlayer($dominoTile, $firstPlayer)) {
                continue;
            }
            if ($this->assignTileToPlayer($dominoTile, $secondPlayer)) {
                continue;
            }
            if ($this->assignTileToStack($dominoTile)) {
                continue;
            }
            $dominoTile->setOnBoard();
        }

        $this->boardProperty->setValue($this->gameMaster, $board);

        // First player plays <6:0> to connect to tile <1:6> on the board
        // Board is now: <1:6> <6:0>
        $this->messageLogMock->expects($this->at(0))->method('logDominoTileConnectedMessage');
        $this->gameMaster->continue();
        $this->assertCount(1, $this->dominoes->getForPlayer($firstPlayer));
        $this->assertSame(1, $board->getFirstTileEnd());
        $this->assertSame(null, $board->getSecondTileEnd());

        // Player 2 can't play, draws tile <4:1>
        // Player 2 plays <4:1> to connect to tile <1:6> on the board
        // Board is now: <4:1> <1:6> <6:0>
        $this->messageLogMock->expects($this->at(1))->method('logPlayerDrawsTileFromStack');
        $this->messageLogMock->expects($this->at(1))->method('logDominoTileConnectedMessage');
        $this->gameMaster->continue();
        $this->assertSame(4, $board->getFirstTileEnd());
        $this->assertSame(null, $board->getSecondTileEnd());

        // First player plays <6:0> to connect to tile <1:6> on the board
        // Board is now: <4:1> <1:6> <6:0> <0:5>
        $this->messageLogMock->expects($this->at(2))->method('logDominoTileConnectedMessage');
        $this->gameMaster->continue();
        $this->assertSame(4, $board->getFirstTileEnd());
        $this->assertSame(5, $board->getSecondTileEnd());
    }

    /**
     * @test
     */
    public function playerShouldWinTheGameWhenDoesNotHaveAnyTilesAndStackIsEmpty(): void
    {
        $winner = new PlayerOwner(self::PLAYER_1);
        $loser = new PlayerOwner(self::PLAYER_2);

        /** @var DominoTile[] $dominoTiles */
        $dominoTiles = $this->dominoTilesProperty->getValue($this->dominoes);
        foreach ($dominoTiles as $tile) {
            if ($tile->getFirstField() === null && $tile->getSecondField() === null) {
                $tile->setOnPlayerPile($winner);
                continue;
            }
            $tile->setOnBoard();
        }

        $board = new Board(new DominoTile(1, 1, new StackTileOwner()));
        $board->addDominoTile(new DominoTile(null, 1, $loser));
        $this->boardProperty->setValue($this->gameMaster, $board);

        $this->gameMaster->continue();
        $this->assertTrue($this->gameMaster->isGameFinished());
        $this->assertTrue($this->gameMaster->getWinner()->isEqualTo($winner));
    }

    /**
     * @test
     */
    public function noOneShouldWinWithNotMatchingDominoesAndEmptyStack(): void
    {
        $firstPlayer = new PlayerOwner(self::PLAYER_1);
        $secondPlayer = new PlayerOwner(self::PLAYER_2);
        /** @var DominoTile[] $dominoTiles */
        $dominoTiles = $this->dominoTilesProperty->getValue($this->dominoes);
        foreach ($dominoTiles as $tile) {
            if ($tile->getFirstField() === null && $tile->getSecondField() === null) {
                $tile->setOnPlayerPile($firstPlayer);
                continue;
            }
            if ($tile->getFirstField() === 2 && $tile->getSecondField() === 2) {
                $tile->setOnPlayerPile($secondPlayer);
            }
            $tile->setOnBoard();
        }
        $board = new Board(new DominoTile(5, null, new StackTileOwner()));
        $board->addDominoTile(new DominoTile(null, 1, $firstPlayer));
        $this->boardProperty->setValue($this->gameMaster, $board);

        $this->gameMaster->continue();
        $this->assertTrue($this->gameMaster->isGameFinished());
        $this->assertNull($this->gameMaster->getWinner());
    }

    /**
     * @test
     */
    public function shouldHaltProgramOnNonUniquePlayerName(): void
    {
        $this->expectException(NonUniquePlayerNameException::class);
        new GameMaster($this->dominoes, $this->messageLogMock, ...[self::PLAYER_1, self::PLAYER_1]);
    }

    private function setDominoTilesReflectionProperty(): void
    {
        $reflectionClass = new ReflectionClass(Dominoes::class);
        $this->dominoTilesProperty = $reflectionClass->getProperty('tiles');
        $this->dominoTilesProperty->setAccessible(true);
    }

    private function setDominoTileOwnerReflectionProperty(): void
    {
        $reflectionClass = new ReflectionClass(DominoTile::class);
        $this->dominoTileOwnerProperty = $reflectionClass->getProperty('owner');
        $this->dominoTileOwnerProperty->setAccessible(true);
    }

    private function setBoardReflectionProperty(): void
    {
        $reflectionClass = new ReflectionClass(GameMaster::class);
        $this->boardProperty = $reflectionClass->getProperty('board');
        $this->boardProperty->setAccessible(true);
    }

    private function assignTileToPlayer(DominoTile $dominoTile, PlayerOwner $playerOwner): bool
    {
        foreach (self::PLAYER_TILES[$playerOwner->getName()] as $tileData) {
            if (($dominoTile->getFirstField() === $tileData[0] && $dominoTile->getSecondField() === $tileData[1]) ||
                ($dominoTile->getFirstField() === $tileData[1] && $dominoTile->getSecondField() === $tileData[0])) {
                $dominoTile->setOnPlayerPile($playerOwner);
                return true;
            }
        }

        return false;
    }

    private function assignTileToStack(DominoTile $dominoTile): bool
    {
        foreach (self::STACK_TILES as $tileData) {
            if (($dominoTile->getFirstField() === $tileData[0] && $dominoTile->getSecondField() === $tileData[1]) ||
                ($dominoTile->getFirstField() === $tileData[1] && $dominoTile->getSecondField() === $tileData[0])) {
                $this->dominoTileOwnerProperty->setValue($dominoTile, new StackTileOwner());
                return true;
            }
        }

        return false;
    }
}
