<?php

declare(strict_types=1);

namespace CODEfactors\Tests\Domino;

use CODEfactors\Domino\Board;
use CODEfactors\Domino\DominoTile;
use CODEfactors\Domino\TileOwner\BoardTileOwner;
use PHPUnit\Framework\TestCase;

final class BoardTest extends TestCase
{
    /**
     * @test
     */
    public function boardShouldCreateOneDominoTileOnCreation(): void
    {
        $board = new Board(new DominoTile(6, 4, new BoardTileOwner()));
        $this->assertSame(6, $board->getFirstTileEnd());
        $this->assertSame(4, $board->getSecondTileEnd());
        $this->assertCount(1, $board->getTiles());
    }
}
