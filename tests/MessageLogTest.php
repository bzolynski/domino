<?php

declare(strict_types=1);

namespace CODEfactors\Tests\Domino;

use CODEfactors\Domino\Board;
use CODEfactors\Domino\Dominoes;
use CODEfactors\Domino\DominoTile;
use CODEfactors\Domino\MessageLog;
use CODEfactors\Domino\TileOwner\PlayerOwner;
use CODEfactors\Domino\TileOwner\StackTileOwner;
use PHPUnit\Framework\TestCase;

final class MessageLogTest extends TestCase
{
    /**
     * @var MessageLog
     */
    private $messageLog;

    /**
     * @var Board
     */
    private $board;

    public function setUp(): void
    {
        $this->messageLog = new MessageLog();
        $this->board = new Board(new DominoTile(1, 6, new StackTileOwner()));
    }

    /**
     * @test
     */
    public function shouldContainInitialBoardStatusMessage(): void
    {
        $this->messageLog->logInitialBoardStatusMessage($this->board);
        $this->assertSame('Game starts with first tile: <1:6>', $this->messageLog->getMessages()[0]);
    }

    /**
     * @test
     */
    public function shouldContainBoardStatusMessage(): void
    {
        $this->messageLog->logBoardStatusMessage($this->board);
        $this->assertSame('Board is now: <1:6>', $this->messageLog->getMessages()[0]);
    }

    /**
     * @test
     */
    public function shouldContainConnectedTileMessage(): void
    {
        $playerOwner = new PlayerOwner('Alice');
        $this->messageLog->logDominoTileConnectedMessage(
            $playerOwner,
            new DominoTile(1, 5, $playerOwner),
            new DominoTile(1, 6, new StackTileOwner())
        );
        $this->assertSame(
            'Alice plays <1:5> to connect to tile <1:6> on the board',
            $this->messageLog->getMessages()[0]
        );
    }

    /**
     * @test
     */
    public function shouldContainTileDrawnFromTheStackMessage(): void
    {
        $playerOwner = new PlayerOwner('Alice');
        $this->messageLog->logPlayerDrawsTileFromStack(
            $playerOwner,
            new DominoTile(1, 5, $playerOwner)
        );
        $this->assertSame(
            'Alice can\'t play, draws tile <1:5>',
            $this->messageLog->getMessages()[0]
        );
    }

    /**
     * @test
     */
    public function shouldContainWinnerOfTheGameMessage(): void
    {
        $playerOwner = new PlayerOwner('Alice');
        $this->messageLog->logGameWonByPlayer(
            $playerOwner
        );
        $this->assertSame(
            'Player Alice has won!',
            $this->messageLog->getMessages()[0]
        );
    }

    /**
     * @test
     */
    public function shouldContainNoWinnerMessage(): void
    {
        $playerOwner = new PlayerOwner('Alice');
        $dominoes = new Dominoes();
        $dominoTile = $dominoes->drawTileFromStack();
        $dominoTile->setOnPlayerPile($playerOwner);
        $this->messageLog->logNoWinner(
            $dominoes,
            ...[$playerOwner]
        );
        $this->assertSame(
            'Player Alice has ' . $dominoTile->toString(),
            $this->messageLog->getMessages()[0]
        );
        $this->assertSame(
            'No winner',
            $this->messageLog->getMessages()[1]
        );
    }
}
