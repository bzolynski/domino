<?php

declare(strict_types=1);

namespace CODEfactors\Tests\Domino;

use CODEfactors\Domino\DominoTile;
use CODEfactors\Domino\Exception\InvalidFieldValueException;
use CODEfactors\Domino\TileOwner\StackTileOwner;
use PHPUnit\Framework\TestCase;

final class DominoTileTest extends TestCase
{
    /**
     * @test
     * @dataProvider validTiles
     */
    public function dominoTilesAreCorrect(?int $firstField, ?int $secondField): void
    {
        $dominoTile = new DominoTile($firstField, $secondField, new StackTileOwner());
        $this->assertSame($firstField, $dominoTile->getFirstField());
        $this->assertSame($secondField, $dominoTile->getSecondField());
        $this->assertTrue($dominoTile->matchesWith($firstField));
        $this->assertTrue($dominoTile->matchesWith($secondField));
    }

    /**
     * @test
     * @dataProvider invalidTiles
     */
    public function dominoTilesHaveInvalidFields(?int $firstField, ?int $secondField): void
    {
        $this->expectException(InvalidFieldValueException::class);
        new DominoTile($firstField, $secondField, new StackTileOwner());
    }

    /**
     * @test
     */
    public function dominoTilesShouldSwap(): void
    {
        $dominoTile = new DominoTile(4, 6, new StackTileOwner());
        $dominoTile->swap();
        $this->assertSame(6, $dominoTile->getFirstField());
        $this->assertSame(4, $dominoTile->getSecondField());
        $this->assertSame('<6:4>', $dominoTile->toString());
    }

    public function validTiles(): array
    {
        return [
            [1, 2],
            [6, 6],
            [null, 5],
            [null, null]
        ];
    }

    public function invalidTiles(): array
    {
        return [
            [1, 7],
            [-1, 6],
            [null, 0]
        ];
    }
}
